﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
	public PlayerController pc;
	public Health hp;
	public GameObject[] hazards;
	public GameObject debuff;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	private bool gameOver;
	private bool restart;
	private bool warp;
	private int score;
	public Slider scoreSlider;
	public Slider energySlider;
	public Slider healthBar;
	public GameObject warpText;


	void Start () 
	{
		score = 0;
		gameOver = false;
		restart = false;
		warp = false;
		restartText.text = "";
		gameOverText.text = "";
		UpdateScore ();
		StartCoroutine (SpawnWaves ());
		scoreSlider.value = .0f;
		energySlider.value = .0f;
		healthBar.value = hp.health;
	}

	void Update ()
	{
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				SceneManager.LoadScene(0);
			
			}
		}

		if (warp) {
			if (Input.GetKeyDown (KeyCode.T) && SceneManager.GetActiveScene ().buildIndex != 4) {
				int newNumber = SceneManager.GetActiveScene ().buildIndex;
				newNumber += 1;
				if (newNumber <= 5)
					SceneManager.LoadScene (newNumber);
			}
		}

		if (hp != null) {
			healthBar.value = hp.health;
		}

		}



	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++) {
				GameObject hazard = hazards[Random.Range(0, hazards.Length)];
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), hazard.transform.position.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				if (Random.value <= .15f)
					Instantiate (debuff, spawnPosition, spawnRotation);
				else
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) {
				restartText.text = "Press 'R' to Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore ()
	{
			scoreText.text = "Score: " + score;
		if (scoreSlider.value <= 1f && pc.superRate == false) {
			scoreSlider.value += .1f;
			if (scoreSlider.value >= 1f) {
				pc.fireRate = 0;
				pc.superRate = true;
			}
		}

		if (energySlider.value <= 1f) {
			energySlider.value += .03f;
			if (energySlider.value >= 1f) {
				warp = true;
				warpText.SetActive (true);
			}

		}
	}

	public void GameOver ()
	{
		gameOverText.text = "Game Over";
		gameOver = true;

	}
}
