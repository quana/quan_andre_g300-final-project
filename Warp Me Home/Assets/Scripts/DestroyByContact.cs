﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {






	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "Boundary" || other.tag == "Enemy" || other.tag == "Debuff") {
			return;
		}

		if (other.GetComponent < Health > () != null) {
			other.GetComponent < Health > ().TakeDamage (1);
		} else {
			Destroy (other.gameObject);
		}

		if (GetComponent < Health > () != null) {
			GetComponent < Health > ().TakeDamage (1);
		} else {
			Destroy (gameObject);
		}
	}
}
