﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	public float health;
	public GameObject explosion;
	public int scoreValue;
	private GameController gameController;

	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindGameObjectWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent <GameController> ();
		}

		if (gameControllerObject == null) {
			Debug.Log ("Cannot find 'GameController' script");
		}
	}

	// Update is called once per frame
	public void TakeDamage (int damage) {
		health -= damage;
		if (health <= 0f) {
			Death ();
		}
	}

	void Death (){
		if (explosion != null)
		{
			Instantiate (explosion, transform.position, transform.rotation);
		}
		if (tag == "Player") {
			gameController.GameOver ();
		}
		gameController.AddScore (scoreValue);
		Destroy (gameObject);
	}
}
