﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class FinaBossMovement : MonoBehaviour {

	public bool right;
	public float movementSpeed;
	Rigidbody rb;
	float rightBoundry;
	float leftBoundry;


	// Use this for initialization
	void Start () {
		rb = GetComponent <Rigidbody> ();
		right = true;
		rightBoundry = 7f;
		leftBoundry = -1.5f;
	}
	
	// Update is called once per frame
	void Update () {
		if (right) {
			rb.velocity = Vector3.right * movementSpeed * Time.deltaTime; 
			if (transform.position.x > rightBoundry) {
				right = false;
			}
		}else {
			rb.velocity = Vector3.left * movementSpeed * Time.deltaTime; 
			if (transform.position.x < leftBoundry) {
				right = true;
			}
		}
	}

	void OnDestroy () {
		int newNumber = SceneManager.GetActiveScene ().buildIndex;
		newNumber += 1;
		if (newNumber <= 6)
			SceneManager.LoadScene (newNumber);
	}
}
